<?php

namespace App\Http\Controllers;

use App\Balance;
use App\External;
use App\Internal;
use Exception;
use Firebase\JWT\JWK;
use Illuminate\Http\Client\ConnectionException;
use Firebase\JWT\JWT;
use GuzzleHttp\Exception\ConnectException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;

class ExternalController extends Controller
{
    /**
     * @OA\Post(
     *      path="/api/external/receive",
     *      operationId="receiveExternalTransfer",
     *      summary="Accepts external bank transfers",
     *      description="Receives transaction request from 3rd party banks",
     *      tags={"External"},
     *      @OA\RequestBody(
     *          @OA\JsonContent(
     *              @OA\Property(property="jwt", type="string", example="eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c"),
     *           )
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Ok"
     *       ),
     *      @OA\Response(response=400, description="Bad request",
     *          @OA\JsonContent(
     *              @OA\Property(property="error", type="string", example="Invalid account number"),
     *           ),
     *      ),
     *      @OA\Response(response=401, description="Access denied"),
     *      @OA\Response(response=404, description="Resource Not Found"),
     *      @OA\Response(response=405, description="Method not allowed"),
     *      @OA\Response(response=406, description="Bad request",
     *          @OA\JsonContent(
     *              @OA\Property(property="error", type="string", example="Invalid account number"),
     *           ),
     *      ),
     *      @OA\Response(response=500, description="Server Internal error")
     * )
     */
    public function receive(Request $request) {
        $request->validate([
            'jwt' => ['required'],
        ]);

        $jwt = $request->jwt;

        if (!isset($jwt) && empty($jwt)) {
            return Internal::constructResponse('error', 'Missing jwt', 406);
        }

        $algorithm = External::getJwtAlgorithm($jwt);
        $publicKey = External::getApiPublicKey($jwt);

        if(isset($publicKey['error']) && !empty($publicKey['error'])) {
            return Internal::constructResponse('error', $publicKey['error'], 400);
        }

        if(!isset($algorithm['status'])) {
            try {
                $payload = JWT::decode($jwt, JWK::parseKeySet($publicKey), $algorithm);
            } catch (Exception $exception) {
                return Internal::constructResponse('error', 'Invlid public key', 400);
            }

            $accountFrom = isset($payload->accountFrom) ? $payload->accountFrom : '';
            $accountTo = isset($payload->accountTo) ? $payload->accountTo : '';
            $amount = isset($payload->amount) ? $payload->amount : '';
            $explanation = isset($payload->explanation) ? $payload->explanation : '';
            $senderName = isset($payload->senderName) ? $payload->senderName : '';

            $data = [
                'accountFrom' => $accountFrom,
                'accountTo' => $accountTo,
                'amount' => $amount,
                'explanation' => $explanation,
                'senderName' => $senderName,
            ];

            foreach ($data as $item => $value) {
                if(strlen($value) == 0) {
                    return Internal::constructResponse('error', 'Missing values', 406);
                }
            }

            $result = External::updateBalance($data);

            if ($result) {
                if (isset($result['error'])) {
                    return Internal::constructResponse('error', $result['error'], 406);
                }

                return Internal::constructResponse('message', 'Success');
            }
        }

        return Internal::constructResponse('error', 'Unable to get data', 406);
    }

    /**
     * @OA\Get(
     *      path="/api/external/key",
     *      operationId="getPublicKey",
     *      summary="Get bank public key",
     *      tags={"External"},
     *      description="Returns bank public key",
     *      @OA\Response(
     *          response=200,
     *          description="successful operation",
     *          @OA\JsonContent(
     *              @OA\Property(property="keys", type="array",
     *                  @OA\Items(
     *                      @OA\Property(property="kty", type="string", example="RSA"),
     *                      @OA\Property(property="e", type="string", example="AQAB"),
     *                      @OA\Property(property="use", type="string", example="sig"),
     *                      @OA\Property(property="kid", type="string", example="bank"),
     *                      @OA\Property(property="alg", type="string", example="RS256"),
     *                      @OA\Property(property="n", type="string", example="wHuAHVpCuIaykgTwKQJ6gpyvZ76t8SloqHHW-bhYX0hSSDqV1VNs3MnF9Ubre_x5eqpkxMOxqynzFaQfPzmGucBtl5uQbELsRrWOvIAp8u6sunRIMnN3RRR9sjij2k4eYSyhvJjVEs2mUCk9xkHwuRfoEmKzIZbxAE-xR_G-dYIdyXY2hlEZSwCpISiqqyOmvt5khvRvPlMi_Kj01giL30vYmCmOLHYiuDHhu3pD2RsQnvxwfeMDSIorwoom1uxsvRM5ynnl9tMfnca_ZvSXIYpmsg0qACVCK1RvwjYD18XaDUHp46_dOggKZFLmrO8d9oZfgI3AeV4fDYXa6Do_Hw"),
     *                  )
     *              )
     *           ),
     *       ),
     *      @OA\Response(response=400, description="Bad request"),
     *      @OA\Response(response=401, description="Access denied"),
     *      @OA\Response(response=404, description="Resource Not Found")
     * )
     */
    public function exposePublicKey() {
        $publicKey = Storage::disk('keys_path')->get('jwks.json');

        return json_encode($publicKey);
    }
}
