<?php

namespace App\Http\Controllers;

use App\Balance;
use App\Internal;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class RegisterController extends Controller
{
    /**
     * @OA\Put(
     *      path="/api/register",
     *      operationId="register",
     *      summary="Register new user",
     *      description="Register new user, does not generate token upon success",
     *      tags={"auth"},
     *      @OA\RequestBody(
     *          required=true,
     *          description="Pass user credentials",
     *          @OA\JsonContent(
     *              required={"name", "email","password", "password_confirmation"},
     *               @OA\Property(property="name", type="string", example="John Smith"),
     *               @OA\Property(property="email", type="string", format="email", example="user1@mail.com"),
     *               @OA\Property(property="password", type="string", format="password", example="PassWord12345", minLength=8),
     *               @OA\Property(property="password_confirmation", type="string", format="password", example="PassWord12345", minLength=8),
     *          ),
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="successful operation",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="New account has been created"),
     *           ),
     *       ),
     *      @OA\Response(response=400, description="Bad request"),
     *      @OA\Response(response=404, description="Resource Not Found"),
     *      @OA\Response(response=405, description="Method not allowed"),
     *      @OA\Response(
     *          response=422,
     *          description="Invalid information",
     *          @OA\JsonContent(
     *              @OA\Property(property="message", type="string", example="The given data was invalid."),
     *              @OA\Property(property="errors", type="object",
     *                      @OA\Property(property="password", type="array",
     *                          @OA\Items(type="string", example="The password confirmation does not match")
     *                      ),
     *                  ),
     *           ),
     *       ),
     *      @OA\Response(response=500, description="Server internal error"),
     * )
     */
    public function register(Request $request)
    {
        $request->validate([
            'name' => ['required'],
            'email' => ['required', 'email', 'unique:users'],
            'password' => ['required', 'min:8', 'confirmed'],
        ]);

        User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);

        $newUserID = DB::table('users')
            ->where('email', $request->email)
            ->pluck('id')
            ->first();

        Balance::newBalance($newUserID);

        return Internal::constructResponse('message', 'New account has been created, you may now log in');
    }
}
