<?php

namespace App\Http\Controllers;

use App\Balance;
use App\Internal;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;

class OutboundTransactionsController extends Controller
{
    /**
     * Tries to execute registered jobs, deletes jobs if successful
     */
    public static function processJobs() {
        $jobs = DB::table('outbound_transactions')
            ->get();

        if (empty($jobs)) {
            return;
        }

        foreach ($jobs as $job) {
            $data = [
                'accountFrom' => $job->accountFrom,
                'accountTo' => $job->accountTo,
                'senderName' => $job->senderName,
                'amount' => $job->amount,
                'description' => $job->description,
            ];

            if ($job->succeeded) {
                self::deleteJob($data);
                return;
            }

            if ($job->dueTime < time()) {
                self::rollbackTransaction($data);
                return;
            }

            self::executeTransaction($data);
        }

        return;
    }

    /**
     * Rolls back transaction
     *
     * @param array $jobData
     * @return void
     */
    public static function rollbackTransaction($jobData) {
        $queryParmas = [
            ['accountFrom', '=', $jobData['accountFrom']],
            ['accountTo', '=', $jobData['accountTo']],
            ['amount', '=', $jobData['amount']],
            ['description', '=', $jobData['description']],
            ['senderName', '=', $jobData['senderName']],
        ];

        $job = DB::table('outbound_transactions')->where($queryParmas)->first();

        $currentUserBalance = Balance::getBalance($job->accountFrom);
        Balance::updateBalance($job->accountFrom, ($currentUserBalance + $job->amount));
        DB::table('transactions')->where($queryParmas)->update(['status' => 'failed']);

        self::deleteJob($jobData);
        return;
    }

    /**
     * Executes transaction from data passed from processJobs
     *
     * @param array $jobData
     * @return void
     */
    public static function executeTransaction($jobData)
    {
        $bankPrefix = substr($jobData['accountTo'], 0, 3);
        $bankTransactionUrl = DB::table('banks')
            ->where('bankPrefix', '=', $bankPrefix)
            ->pluck('transactionUrl')
            ->first();

        $jwt = Internal::createJWT($jobData);

        $headers = [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer '.$jwt,
        ];

        $hasFailed = Http::withHeaders($headers)->post($bankTransactionUrl)->failed();

        if ($hasFailed) {
            self::logFailedAttempt($jobData);
            return;
        }

        self::deleteJob($jobData);
        return;
    }

    /**
     * Log failed attempt
     */
    public static function logFailedAttempt($jobData) {
        $job = DB::table('outbound_transactions')
            ->where([
                ['accountFrom', '=', $jobData['accountFrom']],
                ['accountTo', '=', $jobData['accountTo']],
                ['amount', '=', $jobData['amount']],
                ['description', '=', $jobData['description']],
                ['senderName', '=', $jobData['senderName']],
            ]);

        $jobAttemptCount = $job->pluck('attempts')->first();

        $job->update([
            'attempts' => ($jobAttemptCount + 1),
        ]);

        return;
    }

    /**
     * Inserts new jobs after failed outbound transaction
     *
     * @param array $jobData
     * @return void
     */
    public static function insertJob($jobData) {
        if (self::checkIfJobExists($jobData)) {
            return;
        }

        DB::table('outbound_transactions')
            ->insert(
                [
                    'accountFrom' => $jobData['accountFrom'],
                    'accountTo' => $jobData['accountTo'],
                    'senderName' => $jobData['senderName'],
                    'amount' => $jobData['amount'],
                    'attempts' => 1,
                    'succeeded' => 0,
                    'dueTime' => time() + (3 * 24 * 60 * 60), // adds 3 days to current time
                ]
            );

        return;
    }

    /**
     * Check if queue'd job exists
     *
     * @param array $json
     * @return boolean
     */
    public static function checkIfJobExists($jobData) {
        $job = DB::table('outbound_transactions')
            ->where([
                ['accountFrom', '=', $jobData['accountFrom']],
                ['accountTo', '=',$jobData['accountTo']]
            ])
            ->first();

        if (!empty($job)) {
            return true;
        }

        return false;
    }

    /**
     * Delete a job entry
     *
     * @param array $jobData
     * @return void
     */
    public static function deleteJob($jobData) {
        $job = DB::table('outbound_transactions')
            ->where([
                ['accountFrom', '=', $jobData['accountFrom']],
                ['accountTo', '=', $jobData['accountTo']],
                ['amount', '=', $jobData['amount']],
                ['description', '=', $jobData['description']],
                ['senderName', '=', $jobData['senderName']],
            ]);

        $job->delete();

        return;
    }
}
