<?php

namespace App\Http\Controllers;

use App\Balance;
use App\External;
use App\Internal;
use App\User;
use Illuminate\Http\Request;


class BalanceController extends Controller
{

    /**
     * @OA\Get(
     *      path="/api/balance",
     *      operationId="getBalance",
     *      summary="Get user balance",
     *      tags={"Internal"},
     *      description="Returns user current balance",
     *      @OA\Response(
     *          response=200,
     *          description="successful operation",
     *          @OA\JsonContent(
     *              @OA\Property(property="balance", type="integer", example=123123),
     *           ),
     *       ),
     *      @OA\Response(response=400, description="Bad request"),
     *      @OA\Response(response=401, description="Access denied"),
     *      @OA\Response(response=404, description="Resource Not Found"),
     *      security={
     *         {"bearer": {}}
     *      }
     * )
     */
    public function balance(Request $request) {
        $accountNr = Balance::getAccountNumber($request->user()->id);
        $balance = Balance::getBalance($accountNr);

        return Internal::constructResponse('balance', intval($balance));
    }

    /**
     * @OA\Put(
     *      path="/api/balance/transfer",
     *      operationId="transferFunds",
     *      summary="Transfer funds",
     *      description="Transfer funds from one account to another",
     *      tags={"Transactions"},
     *      @OA\RequestBody(
     *          required=true,
     *          description="Pass transfer information",
     *          @OA\JsonContent(
     *              required={"amount","receiverNumber"},
     *              @OA\Property(property="amount", type="integer", example="432"),
     *              @OA\Property(property="receiverNumber", type="string", example="BYXXXXXXXX"),
     *              @OA\Property(property="description", type="string", example="This is a description"),
     *          ),
     *      ),
     *      @OA\Response(
     *          response=200,
     *          description="Ok"
     *       ),
     *      @OA\Response(response=400, description="Bad request",
     *          @OA\JsonContent(
     *              @OA\Property(property="error", type="string", example="Bad request"),
     *           ),
     *      ),
     *      @OA\Response(response=401, description="Access denied"),
     *      @OA\Response(response=404, description="Resource Not Found"),
     *      @OA\Response(response=500, description="Server Internal error"),
     *      security={
     *         {"bearer": {}}
     *      }
     * )
     */
    public function transfer(Request $request)
    {
        $request->validate([
            'amount' => ['required'],
            'receiverNumber' => ['required'],
            'description' => ['required'],
        ]);

        $amount = $request->amount;
        $userAccountNumber = Balance::getAccountNumber($request->user()->id);
        $senderName = Balance::getSenderName($request->user()->id);
        $userBalance = Balance::getBalance($userAccountNumber);
        $target = $request->receiverNumber;
        $targetBankPrefix = substr($target, 0, 3);
        $description = $request->description;

        if ($targetBankPrefix == Balance::$bankPrefix) {
            $targetBalance = Balance::getBalance($target);

            if(!Balance::verifyAmount($amount) || $userAccountNumber == $target) {
                return Internal::constructResponse('error', 'Bad Request', 400);
            }

            if($userBalance >= $amount) {
                Balance::updateBalance($userAccountNumber, ($userBalance - $amount));
                Balance::updateBalance($target, ($targetBalance + $amount));
                Balance::logTransaction($userAccountNumber, $target, $amount, $description, $senderName);

                return Internal::constructResponse('newBalance', Balance::getBalance($userAccountNumber));
            }

            return Internal::constructResponse('error', 'Insufficient funds', 400);
        }

        if (!empty(Internal::findBank($targetBankPrefix))) {
            return External::executeTransaction($userAccountNumber, $target, $senderName, $amount, $targetBankPrefix, $description);
        }

        $hasSucceeded = Internal::compareBankLists();

        if (!$hasSucceeded) {
            return Internal::constructResponse('error', 'Could not find the bank', 404);
        }

        if (!empty(Internal::findBank($targetBankPrefix))) {
            return External::executeTransaction($userAccountNumber, $target, $senderName, $amount, $targetBankPrefix, $description);
        }

        return Internal::constructResponse('error', 'Could not find the bank', 404);
    }
}
