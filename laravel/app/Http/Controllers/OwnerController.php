<?php

namespace App\Http\Controllers;

use App\Owner;

use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class OwnerController extends Controller
{
    /**
     * @OA\Get(
     *      path="/api/details",
     *      operationId="getDetails",
     *      summary="Get user details",
     *      description="Returns current user details",
     *      tags={"User"},
     *      @OA\Response(
     *          response=200,
     *          description="successful operation",
     *          @OA\JsonContent(
     *              @OA\Property(property="name", type="string", example="John Doe"),
     *              @OA\Property(property="email", type="string", format="email", example="John Doe"),
     *              @OA\Property(property="accountNumber", type="string", example="EL0123456789"),
     *              @OA\Property(property="balance", type="integer", example="1234654"),
     *              @OA\Property(property="transactions", type="object",
     *                      @OA\Property(property="affectedAccount", type="string", example="EL0123456789"),
     *                      @OA\Property(property="amount", type="integer", example="1"),
     *                      @OA\Property(property="time", type="string", example="2020-10-02 08:18:24"),
     *                      @OA\Property(property="action", type="string", example="sent"),
     *                      @OA\Property(property="description", type="string", example="Loan payback"),
     *                  ),
     *           ),
     *       ),
     *      @OA\Response(response=400, description="Bad request"),
     *      @OA\Response(response=404, description="Resource Not Found"),
     *      @OA\Response(response=405, description="Method not allowed"),
     *      @OA\Response(response=402, description="Wrong credentials"),
     *      security={
     *         {"bearer": {}}
     *      }
     * )
     */
    public function getDetails(Request $request) {
        $details = Owner::getDetails($request->user()->id);

        return $details;
    }
}
