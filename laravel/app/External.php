<?php

namespace App;

use App\Http\Controllers\OutboundTransactionsController;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;

class External
{
    /**
     * Updates target user balance with amount
     *
     * @param array $data
     * @return void
     */
    public static function updateBalance($data)
    {
        $accountFrom = $data['accountFrom'];
        $accountTo = $data['accountTo'];
        $amount = $data['amount'];
        $explanation = $data['explanation'];
        $senderName = $data['senderName'];

        if(Balance::validateAccountNumber($accountTo)) {
            if(Balance::verifyAmount($amount)) {
                $currentBalance = Balance::getBalance($accountTo);

                Balance::updateBalance($accountTo, ($currentBalance+$amount));
                Balance::logTransaction($accountFrom, $accountTo, $amount, $explanation, $senderName);

                return true;
            }

            return [
                'error' => 'Invalid amount',
            ];
        }

        return [
            'error' => 'Account number does not exist',
        ];
    }

    /**
     * Get JWT decode algorithm
     *
     * @param array $jwt
     * @return array
     */
    public static function getJwtAlgorithm($jwt) {
        $jwtData = explode('.', $jwt);

        $header = base64_decode($jwtData[0]);
        $header = json_decode($header);

        if (isset($header->alg) && !empty($header->alg)) {
            return [
                $header->alg
            ];
        }

        return [
            'status' => false
        ];
    }

    /**
     * Get Bank prefix from accountFrom number in JWT
     *
     * @param array $jwt
     * @return array
     */
    public static function getBankPrefixFromJwt($jwt) {
        $jwtData = explode('.', $jwt);

        $payload = base64_decode($jwtData[1]);
        $payload = json_decode($payload);

        if (isset($payload->accountFrom) && !empty($payload->accountFrom)) {
            $prefix = substr($payload->accountFrom, 0, 3);

            return [
                'prefix' => $prefix,
            ];
        }

        return [
            'error' => 'Could not find account from number',
        ];
    }

    /**
     * Get public key
     *
     * @param array $jwt
     * @return array
     */
    public static function getApiPublicKey($jwt) {
        $prefix = self::getBankPrefixFromJwt($jwt);

        $bank = Internal::findBank($prefix);
        $apiUrl = isset($bank->jwksUrl) ? $bank->jwksUrl : null;

        if (isset($apiUrl) && !empty($apiUrl)) {
            $publicJson = Http::get($apiUrl)->body();
            $publicJson = json_decode($publicJson);

            $publicKey = $publicJson;

            return $publicKey;
        }

        $checkCentralBank = self::getFromCentralBank($prefix['prefix']);

        if($checkCentralBank) {
            $bank = Internal::findBank($prefix);
            $apiUrl = isset($bank->jwksUrl) ? $bank->jwksUrl : null;
            $publicJson = Http::get($apiUrl)->body();
            $publicJson = json_decode($publicJson);

            $publicKey = $publicJson;

            return $publicKey;
        }

        return [
            'error' => 'Bank does not exist',
        ];
    }

    /**
     * Compare bank list to central bank and find id
     *
     * @param string $prefix
     * @return bool
     */
    public static function getFromCentralBank($prefix) {
        $hasSucceeded = Internal::compareBankLists();

        if (!$hasSucceeded) {
            return false;
        }

        $bank = Internal::findBank($prefix);

        if (isset($bank['error'])) {
            return false;
        }

        return true;
    }

    public static function executeTransaction($accountFrom, $accountTo, $senderName, $amount, $targetBankPrefix, $description) {
        $bankTransactionUrl = DB::table('banks')
            ->where('bankPrefix', '=', $targetBankPrefix)
            ->pluck('transactionUrl')
            ->first();

        $payload = [
            'accountFrom' => $accountFrom,
            'accountTo' => $accountTo,
            'senderName' => $senderName,
            'amount' => $amount,
            'description' => $description
        ];

        $jwt = Internal::createJWT($payload);

        $currentBalance = Balance::getBalance($accountFrom);
        if (!Balance::verifyAmount($amount) || $currentBalance < $amount) {
            if ($currentBalance < $amount) {
                $response = [
                    'message' => 'Insufficient funds',
                ];

                return Response($response, 400);
            }

            $response = [
                'message' => 'Bad request',
            ];

            return Response($response, 400);
        }

        Balance::updateBalance($accountFrom, ($currentBalance-$amount));

        $headers = [
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer '.$jwt,
        ];

        $hasFailed = Http::withHeaders($headers)->post($bankTransactionUrl)->failed();

        if ($hasFailed) {
            OutboundTransactionsController::insertJob($payload);
            Balance::logTransaction($accountFrom, $accountTo, $amount, $description, $senderName, 'pending');
            return Response(['message' => 'Started transaction']);
        }

        Balance::logTransaction($accountFrom, $accountTo, $amount, $description, $senderName);

        return [
            'message' => 'Transaction successful',
        ];
    }
}
