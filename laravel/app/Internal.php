<?php

namespace App;

use Firebase\JWT\JWT;
use Illuminate\Http\Client\ConnectionException;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class Internal
{
    /**
     * Get central bank API key
     *
     * @return string
     */
    public static function getCentralBankApiKey() {
        $apiKey = DB::table('apikeys')
            ->limit(1)
            ->pluck('apiKey')
            ->first();

        return $apiKey;
    }

    /**
     * Get bank list from central bank
     *
     * @return array
     */
    public static function getBankListFromCentralBank() {
        $url = env('CENTRAL_URL');
        $apiKey = self::getCentralBankApiKey();

        try {
            $bankList = Http::withHeaders(['api-key' => $apiKey])->get($url);
        } catch (ConnectionException $error) {
            return [
                'error' => 'Could not fetch list from central bank',
            ];
        }

        $bankListDecoded = json_decode($bankList);

        return $bankListDecoded;
    }

    /**
     * Compares current list with central bank list, if not existing , inserts new bank
     *
     * @return bool
     */
    public static function compareBankLists() {
        $centralBankList = self::getBankListFromCentralBank();

        if ($centralBankList['error']) {
            return false;
        }

        foreach ($centralBankList as $centralBankItemRow) {
            $centralBankItemName = isset($centralBankItemRow->name) ? $centralBankItemRow->name : '';
            $centralBankItemOwners = isset($centralBankItemRow->owners) ? $centralBankItemRow->owners : '';
            $centralBankItemApiKey = isset($centralBankItemRow->apiKey) ? $centralBankItemRow->apiKey : '';
            $centralBankItemJwksUrl = isset($centralBankItemRow->jwksUrl) ? $centralBankItemRow->jwksUrl : '';
            $centralBankItemTransactionUrl = isset($centralBankItemRow->transactionUrl) ? $centralBankItemRow->transactionUrl : '';
            $centralBankItemBankPrefix = isset($centralBankItemRow->bankPrefix) ? $centralBankItemRow->bankPrefix : '';

            $centralBankItem = [
                'name' => $centralBankItemName,
                'owners' => $centralBankItemOwners,
                'apiKey' => $centralBankItemApiKey,
                'jwksUrl' => $centralBankItemJwksUrl,
                'transactionUrl' => $centralBankItemTransactionUrl,
                'bankPrefix' => $centralBankItemBankPrefix,
            ];

            self::insertBankEntryIfMissing($centralBankItem);
        }

        return true;
    }

    /**
     * Check if specific bank entry exits, enter if not
     *
     * @param array $item
     * @return void
     */
    public static function insertBankEntryIfMissing($item) {
        $bankItem = DB::table('banks')
            ->where([
                ['name', '=', $item['name']],
                ['owners', '=', $item['owners']],
                ['apiKey', '=', $item['apiKey']],
                ['jwksUrl', '=', $item['jwksUrl']],
                ['transactionUrl', '=', $item['transactionUrl']],
                ['bankPrefix', '=', $item['bankPrefix']],
            ])
            ->first();

        if(!isset($bankItem) && empty($bankItem)) {
            self::insertNewBank($item);
            return;
        }

        return;
    }

    /**
     * Finds bank based on prefix
     *
     * @param string $prefix
     * @return mixed
     */
    public static function findBank($prefix) {
        $bank = DB::table('banks')
            ->where('bankPrefix', '=', strtolower($prefix))
            ->first();

        if (!empty($bank)) {
            return $bank;
        }

        return [
            'error' => 'Bank does not exist'
        ];
    }

    /**
     * Inserts new bank entry
     *
     * @param array $bank
     * @return bool
     */
    public static function insertNewBank($bank = []) {
        foreach ($bank as $item => $value) {
            if(strlen($value) == 0) {
                return false;
            }
        }

        DB::table('banks')
            ->insert($bank);

        return true;
    }

    /**
     * Compiles data to JWT
     *
     * @param array $payload
     * @return JWT
     */
    public static function createJWT($payload) {
        $privateKey = Storage::disk('keys_path')->get('private.key');;

        return JWT::encode($payload, $privateKey, 'RS256');
    }

    /**
     * Constructs error message
     *
     * @param string $key
     * @param mixed $message
     * @param integer $returnCode
     * @return array
     */
    public static function constructResponse($key, $message, $returnCode = 200) {
        $constructedMessage = [
            $key => $message,
        ];

        return Response($constructedMessage, $returnCode);
    }
}
