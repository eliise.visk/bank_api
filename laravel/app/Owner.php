<?php

namespace App;

use Illuminate\Support\Facades\DB;

class Owner
{
    /**
     * Get user details
     *
     * @param integer $id
     * @return array
     */
    public static function getDetails($id) {
        $user = DB::table('users')
        ->where('id', "=", $id);

        $balance = DB::table('balance')
        ->where('userId', "=", $id);

        return [
            'name' => $user->pluck('name')->first(),
            'email' => $user->pluck('email')->first(),
            'accountNumber' => $balance->pluck('accountNumber')->first(),
            'balance' => $balance->pluck('balance')->first(),
            'transactions' => self::getTransactionLogs($id),
        ];
    }

    /**
     * Get user transaction logs
     *
     * @param integer $id
     * @return array
     */
    public static function getTransactionLogs($id) {
        $accountNumber = Balance::getAccountNumber($id);
        $logs = [];

        $rawData = DB::table('transactions')
            ->where('accountFrom', '=', $accountNumber)
            ->get();

        foreach ($rawData as $dataRow) {
            $logs[] = [
                'affectedAccount' => $dataRow->accountTo,
                'amount' => $dataRow->amount,
                'time' => $dataRow->time,
                'action' => $dataRow->action,
                'description' => $dataRow->description,
            ];
        }

        return $logs;
    }
}
