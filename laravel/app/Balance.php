<?php

namespace App;

use Illuminate\Support\Facades\DB;

class Balance
{
    /**
     * Our bank prefix set in .env
     *
     * @var string
     */
    public static $bankPrefix = 'feb';

    /**
     * Get user balance with account number
     *
     * @param string $accountNumber
     * @return integer
     */
    public static function getBalance($accountNumber) {
        $balance = DB::table('balance')
            ->where('accountNumber', '=', $accountNumber)
            ->pluck('balance')
            ->first();

        return $balance;
    }

    /**
     * Generates new balance account for user
     *
     * @param integer $id
     * @return bool
     */
    public static function newBalance($id) {
        DB::table('balance')->insert(
            [
                'userId' => $id,
                'accountNumber' => Balance::generateNewAccountNumber(),
                'balance' => 0,
            ],
        );

        return true;
    }

    /**
     * Get account number with user ID
     *
     * @param integer $id
     * @return string
     */
    public static function getAccountNumber($id) {
        $accountNumber = DB::table('balance')
            ->where('userId', '=', $id)
            ->pluck('accountNumber')
            ->first();

        return $accountNumber;
    }

    /**
     * Update balance for account
     *
     * @param string $accountNumber
     * @param integer $newBalance
     * @return void
     */
    public static function updateBalance($accountNumber, $newBalance) {
        DB::table('balance')
        ->where('accountNumber', "=", $accountNumber)
        ->update([
            "balance" => $newBalance,
        ]);
    }

    /**
     * Generates new account number
     *
     * @return integer
     */
    public static function generateNewAccountNumber() {
        $randomNumber = '';

        for ($i=0; $i < 8 ; $i++) {
            $randomNumber .= rand(0, 9);
        }

        $randomNumber = Balance::$bankPrefix.$randomNumber;

        if(!Balance::validateAccountNumber($randomNumber)) {
            return $randomNumber;
        }

        Balance::generateNewAccountNumber();
    }

    /**
     * Validates generated account number
     *
     * @param string $accountNumber
     * @return bool
     */
    public static function validateAccountNumber($accountNumber) {
        $balance = DB::table('balance')
            ->where('accountNumber', '=', $accountNumber)
            ->pluck('balance')
            ->first();

        if (!empty($balance)) {
            return true;
        }

        return false;
    }

    /**
     * Verifies if amount is legitimate
     *
     * @param integer
     * @return bool
     */
    public static function verifyAmount($amount) {
        if ($amount > 0) {
            return true;
        }

        return false;
    }

    /**
     * Log transaction for sender and receiver
     *
     * @param string $accountFrom
     * @param string $accountTo
     * @param integer $amount
     * @param string $description
     * @param boolean $status = 'succeeded'
     * @return void
     */
    public static function logTransaction($accountFrom, $accountTo, $amount, $description, $senderName, $status = 'succeeded') {
        $data = [
            [
                'accountFrom' => $accountFrom,
                'accountTo' => $accountTo,
                'amount' => $amount,
                'action' => 'sent',
                'description' => $description,
                'time' => now(),
                'senderName' => $senderName,
                'status' => $status,
            ],
            [
                'accountFrom' => $accountTo,
                'accountTo' => $accountFrom,
                'amount' => $amount,
                'action' => 'received',
                'description' => $description,
                'time' => now(),
                'senderName' => $senderName,
                'status' => $status,
            ],
        ];

        DB::table('transactions')->insert($data);
    }

    /**
     * Get sender name based on id
     *
     * @param integer $id
     * @return string
     */
    public static function getSenderName($id) {
        $senderName = DB::table('users')
            ->where('id', '=', $id)
            ->pluck('name')
            ->first();

        return $senderName;
    }
}
