# Bank API

A top secret api ai for banking in work.

## Table of Contents
  1. [Technical information](#Technical-information)
  1. [How to use](#how-to-use)
  1. [Token](#token)
  1. [Routes](#routes)

for file watching run

```
  npm run watch-poll
```
  <hr>

## Technical information
  * This api is build upon Laravel <b>7.27.0</b>, so read their documantion first: <a href="https://laravel.com/docs/7.x">Here</a>
  * Laravel uses composes, which can be found <a href="https://getcomposer.org/">Here</a>
  * Laravel uses authentication via <a href="https://laravel.com/docs/7.x/passport">passport</a>
  * We also use NPM for Front end, npm comes with <a href="https://nodejs.org/en/">Node.js</a>

  **[⬆ back to top](#table-of-contents)**

  <hr>

## How to use
  * Make sure you have all required dependencies for the api
    * Laravel
    * Composer

  To install all composer dependencies:
  ```
    composer install
  ```

  Once composer has finished installing required dependencies, install passport keys

  ```
    php artisan passport:install
  ```

  Once you have generated new keys, find them and update .env file as follows:

  ```
  PASSPORT_PERSONAL_ACCESS_CLIENT_ID="id from clients here without quotation marks"
  PASSPORT_PERSONAL_ACCESS_CLIENT_SECRET="secret from clients here without quotation marks"
  ```

  Launch test server with:
  ```
   php artisan serve
  ```

  **[⬆ back to top](#table-of-contents)**

  <hr>

## Token
  Token is generated upon login and destroyed upon logout.

  Token needs to be in header as follows:
  ```
  Authorization: 'Bearer ${token}'
  ```

  Token is used to validate further requests after logging in, providing access to otherwise restricted areas

  **[⬆ back to top](#table-of-contents)**

  <hr>

## Routes
  ### Route documentation is generated via L5Swagger and uses swagger to provide documentation

  ```
    composer require "darkaonline/l5-swagger"
  ```

  ### Then

  ```
    php artisan vendor:publish --provider "L5Swagger\L5SwaggerServiceProvider"
  ```
  Configure as needed

  ### To generate documentation

  ```
    php artisan l5-swagger:generate
  ```

  ### Documentation link
  ```
    yourhost.domain/api/documentation
  ```

  ### example api key entry
  You should register bank via central bank
  ```
  "name": "EL Bank",
  "apiKey": "4a0a2184-a974-40f0-9390-4730a640fa69",
  "transactionUrl": "https://lkcdev.tk/api/laravel/public/api/external/receive",
  "bankPrefix": "ec5",
  "owners": "E & L",
  "jwksUrl": "https://lkcdev.tk/api/laravel/public/api/external/key"
  ```

  **[⬆ back to top](#table-of-contents)**
  <hr>
have fun
