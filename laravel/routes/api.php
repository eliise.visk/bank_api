<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

/**
 * Login/Register Routes
 */
Route::post('/login', 'LoginController@login');
Route::post('/logout', 'LoginController@logout')->middleware('auth:api');
Route::put('/register', 'RegisterController@register');

/**
 * Balance Routes
 */
Route::get('/balance', 'BalanceController@balance')->middleware('auth:api');
Route::get('/details', 'OwnerController@getDetails')->middleware('auth:api');

// Transfer
Route::put('/balance/transfer', 'BalanceController@transfer')->middleware('auth:api');

/**
 * External routes for other banks
 */
Route::post('/external/receive', 'ExternalController@receive');
Route::get('/external/key', 'ExternalController@exposePublicKey');
