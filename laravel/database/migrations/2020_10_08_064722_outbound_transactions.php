<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class OutboundTransactions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('outbound_transactions', function (Blueprint $table) {
            $table->id();
            $table->string('accountFrom');
            $table->string('accountTo');
            $table->string('senderName');
            $table->float('amount', 20, 2);
            $table->text('description');
            $table->integer('attempts');
            $table->boolean('succeeded');
            $table->bigInteger('dueTime');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('outbound_transactions');
    }
}
